extends Control

export(PackedScene) var StartScene: PackedScene

var dir: = Directory.new()
var save_names: Array

func _ready():
	$SavesList.hide()

	dir.open("user://")
	if not dir.dir_exists("saves"):
		dir.make_dir("saves")
	dir.change_dir("saves")
	get_saves()

	if save_names.size() == 0:
		$Buttons/Load.disabled = true
	else:
		$Buttons/Load.disabled = false

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		$SavesList.hide()

func get_saves():
	save_names.clear()
	dir.list_dir_begin(true, true)
	var file_name: = dir.get_next()
	while file_name != "":
		if not dir.current_is_dir():
			if file_name.get_extension() == "scn":
				save_names.append(file_name)
		file_name = dir.get_next()

func _on_Play_pressed():
	get_tree().change_scene_to(StartScene)

func _on_Load_pressed():
	get_saves()
	$SavesList.clear()
	for save in save_names:
		$SavesList.add_item(save)
	$SavesList.show()

func _on_SavesList_item_activated(index: int):
	var save_file: String = save_names[index]
	$SavesList.hide()
	var load_scene: = load("user://saves/" + save_file)
	var saved_scene = load_scene.instance()
	get_tree().change_scene_to(load_scene)

func _on_SavesList_item_rmb_selected(index: int, at_position: Vector2):
	$SavesList/DeleteMenu.popup_centered()
	$SavesList/DeleteMenu.rect_global_position = $SavesList.rect_global_position + at_position * $SavesList.rect_scale

func _on_Credits_pressed():
	$CreditsDialog.popup_centered()

func _on_Quit_pressed():
	get_tree().quit()


func _on_DeleteMenu_id_pressed(id: int) -> void:
	pass # Replace with function body.
