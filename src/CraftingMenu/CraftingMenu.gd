extends WindowDialog

export(String) var add_button_group: = "add_buttons"

func _ready():
	Alerts.set_alert("TIP: Press `ESC` to exit bridge when you are near a Planet's surface")
	set_process_input(false)
	for btn in get_tree().get_nodes_in_group(add_button_group):
		btn.connect("pressed", self, "on_craft_item", [btn.get_parent()])

func _input(event):
	if event.is_action_pressed("crafting_menu_close"):
		for player in PlayerRegistry.registry:
			player.set_physics_process(true)
			player.set_process_unhandled_input(true)
		set_process_input(false)
		hide()

func open_menu():
	set_process_input(true)
	for player in PlayerRegistry.registry:
		player.set_physics_process(false)
		player.set_process_unhandled_input(false)
	popup_centered()

func on_craft_item(item_entry: Node):
	var has_ores: = []
	var is_miner: bool = item_entry.item_name == "Miner"

	var available_ores: Dictionary
	for ore in PlayerRegistry.ores:
		available_ores[ore.name] = ore.count

	var available_items: PoolStringArray
	if not is_miner:
		for item in PlayerRegistry.items:
			available_items.append(item.name)

	for rsrc in item_entry.resources.keys():
		if available_ores.keys().has(rsrc):
			if available_ores[rsrc] >= item_entry.resources[rsrc]:
				has_ores.append(true)
			else:
				has_ores.append(false)
		else:
			has_ores.append(false)

	if not has_ores.has(false):
		if not is_miner:
			if Array(available_items).has(item_entry.name):
				for it in PlayerRegistry.items:
					if it.name == item_entry.name:
						it.count += 1
			else:
				PlayerRegistry.add_item(item_entry.name, 1)
		else:
			PlayerRegistry.add_miner()

		for res in item_entry.resources.keys():
			for ore in PlayerRegistry.ores:
				if ore.name == res:
					ore.count -= item_entry.resources[res]
	else:
		print("Insufficient resources")
		Alerts.set_alert("Insufficient resources")
