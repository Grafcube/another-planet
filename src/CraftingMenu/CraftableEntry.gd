extends HBoxContainer
class_name CraftableEntry

export(String) var item_name: = "null" setget set_item_name
func set_item_name(p_name):
	item_name = p_name
	$Name.text = p_name

export(Dictionary) var resources: = {} setget set_resources
func set_resources(p_rsrc):
	var cost_label: PoolStringArray
	resources = p_rsrc
	for res in p_rsrc.keys():
		cost_label.append(String(p_rsrc[res]) + " " + res)
	$CostLabel.text = String(cost_label)

var quantity: = 0 setget set_qty
func set_qty(p_qty):
	quantity = p_qty
	$Quantity.text = "x" + String(quantity)

func _ready():
	for item in PlayerRegistry.items:
		if item.name == item_name:
			$Add.texture_normal = item.name

	$Add.texture_normal = IconRegistry.registry[item_name]

func _process(_delta):
	if item_name == "Miner":
		set_qty(PlayerRegistry.miners.size())
	else:
		for item in PlayerRegistry.items:
			if item.name == item_name:
				set_qty(item.count)
