using System.Collections.Generic;
using Godot;

namespace AnotherPlanet
{
	public class Miner : Node2D
	{
		#region Variables
		/// <summary>
		/// Planet
		/// </summary>
		private ConstructionMaterial ActiveOre { get; set; }

		public string activeOreName;

		/// <summary>
		/// Player
		/// </summary>
		private Resource TargetOre { get; set; }

		[Export]
		public PackedScene minerConfigUI;

		[Export]
		public Texture ActiveSprite;

		[Export]
		public Texture InactiveSprite;

		[Export]
		public float MiningSpeed = 1f; // Time per ore

		private Timer OreTimer;
		private Sprite MinerSprite;

		private bool _isActive = false;
		[Export]
		public bool IsActive
		{
			get { return _isActive; }
			set
			{
				_isActive = value;
				if (value)
				{
					OreTimer.Start();
				}
				else
				{
					OreTimer.Stop();
				}
			}
		}

		[Export]
		public string PlayerGroup = "user";

		[Export]
		public string AreaGroup = "planet_field";

		private Player Target;
		private Planet OreOwner;
		#endregion

		#region Overrides
		public override void _Ready()
		{
			OreTimer = (Timer)GetNode(new NodePath("OreTimer"));
			OreTimer.WaitTime = MiningSpeed;
			Target = (Player)GetTree().GetNodesInGroup(PlayerGroup)[0];
			MinerSprite = (Sprite)GetNode("Sprite");
		}

		public override void _Process(float delta)
		{
			if (IsActive)
			{
				MinerSprite.Texture = ActiveSprite;
			}
			else
			{
				MinerSprite.Texture = InactiveSprite;
			}
		}
		#endregion

		#region Functions
		public void PlaceMiner(Planet targetPlanet)
		{
			GlobalPosition = Target.GlobalPosition;
			GlobalRotation = Target.GlobalRotation;
			GlobalScale = Target.GlobalScale;
			OreOwner = targetPlanet;
			var newUI = minerConfigUI.Instance();
			AddChild(newUI);
			newUI.Call("get_ore", OreOwner, this);
		}

		public void ConfigureMiner()
		{
			var newUI = minerConfigUI.Instance();
			AddChild(newUI);
			newUI.Call("configure_miner", OreOwner, this);
		}

		public void AssignOre(string aOreName, bool configMode = false)
		{
			#region Planet
			var newOre = OreOwner.Ores.Find(i => i.Name == aOreName);
			ActiveOre = newOre;
			activeOreName = ActiveOre.Name;
			#endregion Planet

			var availableOres = new List<ConstructionMaterial>();

			var availableOresReg = (Godot.Collections.Array)Target.PlayerRegistry.Call("get_ores");
			foreach (Resource o in availableOresReg)
			{
				var newAvailableOre = new ConstructionMaterial
				{
					Name = (string)o.Call("get_name"),
					Count = (int)o.Call("get_count")
				};
				availableOres.Add(newAvailableOre);
			}

			newOre = availableOres.Find(i => i.Name == aOreName);

			if (newOre == null)
			{
				TargetOre = (Resource)Target.PlayerRegistry.Call("add_ore", aOreName, 0);
			}
			else
			{
				foreach (Resource o in availableOresReg)
				{
					if ((string)o.Call("get_name") == newOre.Name)
					{
						TargetOre = o;
						break;
					}
				}
			}

			if (!configMode)
			{
				var aMiners = (Godot.Collections.Array)Target.PlayerRegistry.Call("get_miners");
				aMiners.RemoveAt(0);
				IsActive = true;
			}
		}

		public void ResetMiner()
		{
			ActiveOre = default;
			activeOreName = default;
			TargetOre = default;
			OreOwner = default;
		}
		#endregion

		#region Signals
		private void _on_OreTimer_timeout()
		{
			if (ActiveOre.Count > 0)
			{
				ActiveOre.Count--;
				TargetOre.Call("add_count", 1);
			}
			else
			{
				IsActive = false;
			}
		}
		#endregion
	}
}
