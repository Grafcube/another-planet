extends Node2D

onready var Rods: Line2D = get_node("Begin/Rods")

var is_complete: = false
var start_planet: Node2D
var end_planet: Node2D

var start_anchor: Vector2 setget set_begin_anchor
func set_begin_anchor(p_begin: = global_position):
	$Begin.global_position = p_begin
	yield(get_tree().create_timer(0.1), "timeout")
	for ar in $Begin.get_overlapping_areas():
		if ar.is_in_group("planet_ring"):
			start_planet = ar.get_parent()
			break
	start_anchor = p_begin

var end_anchor: Vector2 setget set_end_anchor
func set_end_anchor(p_end: Vector2):
	$End.position = p_end
	$End.monitorable = true
	$End.monitoring = true
	yield(get_tree().create_timer(0.1), "timeout")
	for ar in $End.get_overlapping_areas():
		if ar.is_in_group("planet_ring"):
			end_planet = ar.get_parent()
			break
	end_anchor = p_end

func reset_end_anchor():
	$End.position = $Begin.position
	$End.monitorable = false
	$End.monitoring = false
	end_planet = null
	end_anchor = $Begin.position

func _ready():
	Alerts.set_alert("TIP: Use `W/S` to climb Up and Down")
	Alerts.set_alert("TIP: `LEFT CLICK` to place a Rod when you're at the end of the Bridge")
	Alerts.set_alert("TIP: `RIGHT CLICK` to remove a Rod when you're at the second last point of the Bridge")

func deploy():
	global_position = PlayerRegistry.registry[0].global_position
	set_begin_anchor()
	$Tether.setup()

func enter_bridge():
	$Tether.climb_bridge()

func complete_bridge():
	set_end_anchor(Rods.get_point_position(Rods.get_point_count() - 1) + $Begin.position)
	is_complete = true
	Alerts.set_alert("TIP: The Bridge is now anchored. You cannot build or remove rods on this Bridge")
	Alerts.set_alert("TIP: To edit this Bridge, `RIGHT CLICK` while you are on either Anchor of this Bridge")

func remove_anchor(p_anc: String):
	for item in PlayerRegistry.items:
		if item.name == "Rod":
			item.count += 1
			break
	is_complete = false

	match p_anc:
		"End":
			PlayerRegistry.registry[0].set_deferred("global_rotation", $Tether.global_position.angle_to(end_planet.global_position))
			reset_end_anchor()
			Rods.remove_point(Rods.get_point_count() - 1)
			$Tether.climb_dir(-1)
		"Start":
			var global_rod_points: PoolVector2Array
			for pt in Rods.points:
				global_rod_points.append($Begin.global_position + pt)
			global_rod_points.invert()

			start_planet = end_planet
			$Begin.position = $End.position
			reset_end_anchor()

			Rods.clear_points()

			for pt in global_rod_points:
				Rods.add_point(pt - $Begin.global_position)

			is_complete = false
			$Tether.position = Rods.get_point_position(Rods.get_point_count() - 1)
			Rods.remove_point(Rods.get_point_count() - 1)
			PlayerRegistry.registry[0].global_rotation = $Tether.global_position.angle_to(start_planet.global_position)
			$Tether.position_idx = -1
	print("Bridge is no longer anchored")
	Alerts.set_alert("Bridge is no longer anchored")

func remove_bridge():
	queue_free()
