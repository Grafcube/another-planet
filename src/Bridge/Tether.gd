extends Node2D

export(float, 1, 120) var maximum_rod_length: float

onready var Bridge: Node2D = get_parent()
onready var Player: RigidBody2D = PlayerRegistry.registry[0]
onready var Rods: Line2D = Bridge.get_node("Begin/Rods")
onready var RodPreview: Line2D = $RodPreview

var position_idx: = 0 setget set_tether_pos
func set_tether_pos(pos_idx: int):
	if pos_idx >= 0:
		position_idx = pos_idx
		set_deferred("position", Rods.get_point_position(pos_idx) + Bridge.get_node("Begin").position)
	else:
		position_idx = Rods.get_point_count() + pos_idx
		set_deferred("position", Rods.get_point_position(Rods.get_point_count() + pos_idx) + Bridge.get_node("Begin").position)

var temp_gravity_scale: float
var start_pt: String
var anchor_remove: String

func _ready():
	set_process(false)
	set_process_input(false)

func _input(event):
	$RodPreview.visible = not Bridge.is_complete

	var mouse_intersection: Array
	for ar in $MouseOver.get_overlapping_areas():
		mouse_intersection.append(ar)
	for bd in $MouseOver.get_overlapping_bodies():
		mouse_intersection.append(bd)

	var rod_item: CraftableItem
	for item in PlayerRegistry.items:
		if item.name == "Rod":
			rod_item = item
			break

	if event.is_action_pressed("bridge_rod_place"):
		if rod_item:
			if rod_item.count > 0:
				var is_obstructed: = false

				for nd in mouse_intersection:
					if nd.is_in_group("obstructions"):
						print("Unable to build Rod: Obstructed")
						Alerts.set_alert("Unable to build Rod: Obstructed")
						is_obstructed = true
						break

				if Bridge.is_complete:
					print("Unable to build Rod: Bridge is anchored")
					Alerts.set_alert("Unable to build Rod: Bridge is anchored")
					is_obstructed = true
				elif position_idx != Rods.get_point_count() - 1:
					print("Unable to build Rod: Player not at end of Bridge")
					Alerts.set_alert("Unable to build Rod: Player not at end of Bridge")
					is_obstructed = true

				if not is_obstructed:
					Rods.add_point(get_local_mouse_position().clamped(maximum_rod_length) + Rods.get_point_position(Rods.get_point_count() - 1))
					rod_item.count -= 1
					climb_dir(1)
			else:
				print("Unable to build Rod: Insufficient Resources")
				Alerts.set_alert("Unable to build Rod: Insufficient Resources")
		else:
			print("Unable to build Rod: Insufficient Resources")
			Alerts.set_alert("Unable to build Rod: Insufficient Resources")

	elif event.is_action_pressed("bridge_rod_remove"):
		var can_remove: = false

		if Bridge.is_complete:
			if position_idx == Rods.get_point_count() - 1:
				anchor_remove = "End"
				$AnchorMenu.popup()
				$AnchorMenu.rect_global_position = global_position
				$AnchorMenu.rect_rotation = Player.global_rotation_degrees
			elif position_idx == 0:
				anchor_remove = "Start"
				start_pt = "Start"
				$AnchorMenu.popup()
				$AnchorMenu.rect_global_position = global_position
				$AnchorMenu.rect_rotation = Player.global_rotation_degrees
			else:
				print("Unable to remove Rod: Bridge is anchored")
				Alerts.set_alert("Unable to remove Rod: Bridge is anchored")
		elif position_idx == Rods.get_point_count() - 1:
			print("Unable to remove Rod: Player is at end of Bridge")
			Alerts.set_alert("Unable to remove Rod: Player is at end of Bridge")
		elif position_idx < Rods.get_point_count() - 2:
			print("Unable to remove Rod: Cannot remove more than one at a time")
			Alerts.set_alert("Unable to remove Rod: Cannot remove more than one at a time")
		else:
			Rods.remove_point(Rods.get_point_count() - 1)
			rod_item.count += 1
			climb_dir(-1)

	elif event.is_action_pressed("bridge_climb_begin", true):
		climb_dir(-1)

	elif event.is_action_pressed("bridge_climb_end", true):
		climb_dir(1)

	elif event.is_action_pressed("bridge_exit"):
		var can_exit: = false
		for area in Player.get_node("PlanetMonitor").get_overlapping_areas():
			if area.is_in_group("planet_ring"):
				can_exit = true
				break

		if can_exit:
			if not Bridge.is_complete:
				if Player.CurrentPlanet != null:
					if Bridge.start_planet != Player.CurrentPlanet:
						Bridge.complete_bridge()
				if Rods.get_point_count() == 1:
					Bridge.remove_bridge()
			exit_bridge()
		else:
			print("Unable to exit Bridge: Not on Planet")
			Alerts.set_alert("Unable to exit Bridge: Not on Planet")

func _process(_delta):
	RodPreview.set_point_position(1, get_local_mouse_position().clamped(maximum_rod_length))
	$MouseOver.position = get_local_mouse_position().clamped(maximum_rod_length)
	Player.set_deferred("global_position", global_position)

func setup():
	set_tether_pos(0)
	attach_player()

func climb_bridge():
	for ar in Player.get_node("PlanetMonitor").get_overlapping_areas():
		if ar.is_in_group("bridge_ends"):
			start_pt = ar.name
			break
	match start_pt:
		"Begin":
			set_tether_pos(0)
		"End":
			set_tether_pos(-1)
	attach_player()

func attach_player():
	Player.set_physics_process(false)
	Player.set_process_unhandled_input(false)
	temp_gravity_scale = Player.gravity_scale
	Player.gravity_scale = 0
	Player.get_parent().remove_child(Player)
	add_child(Player)
	Player.global_position = global_position
	set_process(true)
	set_process_input(true)

func climb_dir(p_dir: int):
	if start_pt == "End":
		p_dir = -p_dir
	var new_pos: = position_idx + p_dir
	if new_pos >= 0 and new_pos < Rods.get_point_count():
		set_tether_pos(new_pos)
	elif new_pos < 0:
		set_tether_pos(0)
	elif new_pos >= Rods.get_point_count():
		set_tether_pos(-1)

func exit_bridge():
	RodPreview.set_point_position(1, Vector2.ZERO)
	$MouseOver.global_position = Bridge.global_position
	set_process(false)
	set_process_input(false)
	Player.gravity_scale = temp_gravity_scale
	Player.get_parent().remove_child(Player)
	get_tree().get_nodes_in_group("root")[0].add_child(Player)
	Player.set_deferred("global_position", global_position)
	Player.set_physics_process(true)
	Player.set_process_unhandled_input(true)

func _on_AnchorMenu_id_pressed(id: int):
	match id:
		0:
			Bridge.remove_anchor(anchor_remove)

func _on_AnchorMenu_about_to_show():
	set_process(false)
	set_process_input(false)

func _on_AnchorMenu_popup_hide():
	anchor_remove = ""
	set_process(true)
	set_process_input(true)
