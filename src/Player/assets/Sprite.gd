extends Sprite

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("move_cw"):
		flip_h = false
	elif event.is_action_pressed("move_acw"):
		flip_h = true
