﻿using Godot;
using Godot.Collections;
using static Godot.GD;
using static Godot.Mathf;

namespace AnotherPlanet
{
	public class Player : RigidBody2D
	{
		#region Variables

		#region Config
		[Export]
		public float Speed = 1f;

		[Export]
		private float rotationOffset = Pi / 2;

		[Export]
		public string AreaGroup = "planet_ring";

		[Export]
		public string ObstructionGroup = "obstructions";

		[Export]
		public string MachineGroup = "machine";

		[Export]
		public string BridgeGroup = "bridge_ends";
		#endregion

		#region Node Refs
		[Export]
		public NodePath areaMonitor;
		private Area2D MonitorArea;
		#endregion

		#region Scene Refs
		[Export]
		public PackedScene CraftingScene;
		private Node CraftingMenu;

		[Export]
		public PackedScene BridgeScene;
		#endregion

		#region Checks
		public bool IsInPlanet = false;
		public bool isOverMiner = false;
		public bool isObstructed = false;
		public bool isOverBridge = false;
		#endregion

		#region Properties
		public Node PlayerRegistry;
		public Planet CurrentPlanet;
		public Miner overMiner;
		public Node2D overBridge;
		public Node Alerts;
		#endregion

		#endregion

		#region Overrides
		public override void _Ready()
		{
			PlayerRegistry = GetNode("/root/PlayerRegistry");
			PlayerRegistry.Call("register", this);

			Alerts = GetNode("/root/Alerts");

			CraftingMenu = CraftingScene.Instance();
			var HUD = (Node)GetTree().GetNodesInGroup("gui")[0];
			HUD.AddChild(CraftingMenu);

			MonitorArea = (Area2D)GetNode(areaMonitor);

			PlayerRegistry.Call("add_miner");
		}

		public override void _PhysicsProcess(float delta)
		{
			var direction = Vector2.Zero;
			int angularDirection = 0;
			if (Input.IsActionPressed("move_cw"))
			{
				angularDirection += 1;
			}
			if (Input.IsActionPressed("move_acw"))
			{
				angularDirection -= 1;
			}

			direction.x = (float)angularDirection;

			ApplyCentralImpulse(direction.Rotated(Rotation) * Speed * delta);
			if (CurrentPlanet != null && CurrentPlanet.Name != "null")
			{
				this.Rotation = GlobalPosition.AngleToPoint(CurrentPlanet.GlobalPosition) + rotationOffset;
			}

		}

		public override void _UnhandledInput(InputEvent @event)
		{
			isObstructed = false;

			var obstructions = MonitorArea.GetOverlappingAreas();
			foreach (Node i in obstructions)
			{
				if (i.IsInGroup(ObstructionGroup))
				{
					isObstructed = true;
					break;
				}
			}

			if (@event.IsActionPressed("miner_place") && IsInPlanet)
			{
				var AvailableMiners = (Array)PlayerRegistry.Call("get_miners");
				if (AvailableMiners.Count != 0)
				{
					if (!isObstructed)
					{
						var newMiner = (Miner)AvailableMiners[0];
						CurrentPlanet.GetNode("MinerContainer").AddChild(newMiner, true);
						newMiner.PlaceMiner(CurrentPlanet);
					}
					else
					{
						Print("Unable to deploy Miner: Obstructed");
						Alerts.Call("set_alert", "Unable to deploy Miner: Obstructed");
					}
				}
				else
				{
					Print("No Miners available");
					Alerts.Call("set_alert", "No Miners available");
				}
			}

			if (@event.IsActionPressed("miner_cfg_open") && isOverMiner)
			{
				overMiner.ConfigureMiner();
			}

			if (@event.IsActionPressed("crafting_menu_open"))
			{
				CraftingMenu.Call("open_menu");
			}

			if (@event.IsActionPressed("bridge_build"))
			{
				if (!isObstructed)
				{
					if (IsInPlanet)
					{
						var availableItems = (Dictionary)PlayerRegistry.Call("get_item_dict");
						if (availableItems.Contains("Rod"))
						{
							if ((int)availableItems["Rod"] != 0)
							{
								var newBridge = BridgeScene.Instance();
								GetParent().GetNode("Bridges").AddChild(newBridge);
								newBridge.Call("deploy");
							}
							else
							{
								Print("Unable to deploy Bridge: Insufficient Resources");
								Alerts.Call("set_alert", "Unable to deploy Bridge: Insufficient Resources");
							}
						}
						else
						{
							Print("Unable to deploy Bridge: Insufficient Resources");
							Alerts.Call("set_alert", "Unable to deploy Bridge: Insufficient Resources");
						}
					}
				}
				else
				{
					Print("Unable to deploy Bridge: Obstructed");
					Alerts.Call("set_alert", "Unable to deploy Bridge: Obstructed");
				}
			}

			if (@event.IsActionPressed("bridge_climb") && isOverBridge)
			{
				overBridge.Call("enter_bridge");
			}
		}
		#endregion

		#region Signals
		private void _on_PlanetMonitor_area_entered(Area2D area)
		{
			if (area.IsInGroup(AreaGroup))
			{
				IsInPlanet = true;
				CurrentPlanet = (Planet)area.GetParent();
			}

			if (area.IsInGroup(MachineGroup))
			{
				isOverMiner = true;
				overMiner = (Miner)area.GetParent();
			}

			if (area.IsInGroup(BridgeGroup))
			{
				isOverBridge = true;
				overBridge = (Node2D)area.GetParent();
			}
		}

		private void _on_PlanetMonitor_area_exited(Area2D area)
		{
			if (area.IsInGroup(AreaGroup))
			{
				IsInPlanet = false;
				CurrentPlanet = default;
			}

			if (area.IsInGroup(MachineGroup) && area.GetParent() == overMiner)
			{
				isOverMiner = false;
				overMiner = default;
			}

			if (area.IsInGroup(BridgeGroup) && area.GetParent() == overBridge)
			{
				isOverBridge = false;
				overBridge = default;
			}
		}
		#endregion
	}
}
