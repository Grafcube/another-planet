extends Button

func _on_pressed() -> void:
	Save.save_game()

func _on_mouse_entered() -> void:
	modulate = Color("ffffffff")

func _on_mouse_exited() -> void:
	modulate = Color("c0ffffff")
