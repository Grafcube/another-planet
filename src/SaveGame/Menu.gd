extends Button

export(String, FILE, "*.tscn") var main_menu: String

func _on_pressed() -> void:
	var menu: PackedScene = load(main_menu)
	menu.instance()
	get_tree().change_scene_to(menu)

func _on_mouse_entered() -> void:
	modulate = Color("ffffffff")

func _on_mouse_exited() -> void:
	modulate = Color("c0ffffff")
