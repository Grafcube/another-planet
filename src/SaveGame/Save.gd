extends Node

var save_dir: = "user://saves/"

func save_game():
	var new_save: = PackedScene.new()
	new_save.pack(get_node("/root/Main"))
	var datetime: = OS.get_datetime()
	var save_file: String = String(datetime.year) + "-" + String(datetime.month) + "-" + String(datetime.day) + " " + String(datetime.hour) + "-" + String(datetime.minute) + "-" + String(datetime.second) + ".scn"
	var dir: = Directory.new()
	var res: = dir.open("user://")
	if not dir.dir_exists("saves"):
		dir.make_dir("saves")
	var status: = ResourceSaver.save(save_dir + save_file, new_save, 32)
	print("Save File: ", status)
	if status == OK:
		Alerts.set_alert("GAME SAVED: " + save_file)
