using System.Collections.Generic;
using Godot;

namespace AnotherPlanet
{
	public class Planet : StaticBody2D
	{
		#region Variables
		[Export]
		public string PlanetName = "null";

		[Export]
		public Dictionary<string, int> AvailableOres = new Dictionary<string, int>();

		public List<ConstructionMaterial> Ores = new List<ConstructionMaterial>();
		#endregion

		#region Overrides
		public override void _Ready()
		{
			var NameLabel = (Label)GetNode("Name");
			NameLabel.Text = PlanetName;
			foreach (KeyValuePair<string, int> ore in AvailableOres)
			{
				Ores.Add(new ConstructionMaterial(ore.Key, ore.Value));
			}
		}

		public override void _Process(float delta)
		{
			AvailableOres.Clear();
			foreach (var ore in Ores)
			{
				AvailableOres.Add(ore.Name, ore.Count);
			}
		}
		#endregion
	}
}
