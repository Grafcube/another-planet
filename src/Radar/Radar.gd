extends Node2D

export(float, 0, 12) var scan_delay: = 1.0
export(float, 1, 12) var scan_lifetime: = 8.0
export(float, 0, 12) var cooldown: = 12.0
export(float, 0, 200) var line_max: = 1.0

var is_active: = false
var is_pointing: = false
var planets_in_range: Dictionary # of Planets (StaticBody2D)
var created_lines: Array # of Line2D

func _ready():
	$ActiveSprite.hide()
	$Lines.hide()

func _unhandled_input(event):
	if event.is_action_pressed("radar_scan"):
		scan()

func _physics_process(delta):
	if is_pointing:
		for ln in created_lines:
			var direction: Vector2 = (planets_in_range[ln.name].global_position - global_position).rotated(-global_rotation)
			var line_factor: = direction.length() / line_max
			direction = direction.clamped(line_factor)
			ln.set_point_position(1, direction)

func scan():
	if not is_active:
		is_active = true
		print("Scanning...")
		Alerts.set_alert("Scanning...")
		$ActiveSprite.show()
		yield(get_tree().create_timer(scan_delay), "timeout")
		get_planets()
		set_directions()
		$Lines.show()
		yield(get_tree().create_timer(scan_lifetime), "timeout")
		reset()
	else:
		print("Unable to run Radar: Scan already active")
		Alerts.set_alert("Unable to run Radar: Scan already active")

func get_planets():
	for nd in $ScanArea.get_overlapping_areas():
		if nd.is_in_group("planet_field") and nd.get_parent() != PlayerRegistry.registry[0].CurrentPlanet:
			planets_in_range[nd.get_parent().name] = nd.get_parent()

func set_directions():
	if planets_in_range.keys().size() > 0:
		is_pointing = true
	for planet in planets_in_range.keys():
		var new_line: Line2D = $Lines/Direction.create_instance()
		new_line.name = planet
		created_lines.append(new_line)

func reset():
	$ActiveSprite.hide()
	is_pointing = false
	for ln in created_lines:
		ln.queue_free()
	created_lines.clear()
	planets_in_range.clear()
	is_active = false
	$Lines.hide()
