extends PopupMenu

var target_name: String

func _on_id_pressed(id: int):
	match id:
		0:
			salvage()

func salvage():
	var item_resources: Dictionary
	for i in get_tree().get_nodes_in_group("craftable_items"):
		if i.item_name == target_name:
			item_resources = i.resources
			break

	for res in item_resources.keys():
		PlayerRegistry.add_ore(res, item_resources[res])

	if target_name == "Miner":
		var old_miner: Node = PlayerRegistry.miners.pop_front()
		old_miner.queue_free()
	elif target_name != "":
		PlayerRegistry.add_item(target_name, -1)

func _on_popup_hide():
	target_name = ""
