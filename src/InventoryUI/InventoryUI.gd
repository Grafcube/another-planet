extends Control

export(PackedScene) var ore_button_ui: PackedScene
export(PackedScene) var item_menu_ui: PackedScene

var miner_count: = 0
var setup_delay: = 1.0

onready var MinerCount = $ItemBar/Inventory/Miners/Count
onready var OreBar = $ItemBar/Inventory/Ores
onready var ItemBar = $ItemBar/Inventory/Items

var item_menu: PopupMenu

func _ready():
	item_menu = item_menu_ui.instance()
	add_child(item_menu)
	yield(get_tree().create_timer(setup_delay), "timeout")
	update_miner_count()
	yield(get_tree().create_timer(2.0), "timeout")
	tutorial()

func _process(_delta):
	update_ores_inv()
	update_miner_count()
	update_items_inv()

func tutorial():
	Alerts.set_alert("TIP: Use `A/D` to move Left and Right")
	yield(get_tree().create_timer(6.0), "timeout")
	Alerts.set_alert("TIP: `LEFT CLICK` to deploy your Miner")
	yield(get_tree().create_timer(120.0), "timeout")
	Alerts.set_alert("TIP: Press `V` to scan for Planets in Range")
	yield(get_tree().create_timer(120.0), "timeout")
	Alerts.set_alert("TIP: Press `E` to open the Crafting Menu")
	yield(get_tree().create_timer(300.0), "timeout")
	Alerts.set_alert("TIP: Build Bridges by pressing `F`")
	yield(get_tree().create_timer(300.0), "timeout")
	Alerts.set_alert("TIP: Click on Miners or Items on your Inventory bar below to salvage them back into Ores")

func update_ores_inv():
	for i in OreBar.get_children():
		i.queue_free()
	for ore in PlayerRegistry.ores:
		var new_ore: = ore_button_ui.instance()
		new_ore.name = ore.name
		new_ore.texture_normal = ore.icon
		new_ore.label_name = ore.name
		new_ore.label_count = ore.count
		OreBar.add_child(new_ore)

func update_miner_count():
	if PlayerRegistry.registry.size() > 0:
		miner_count = PlayerRegistry.miners.size()
		MinerCount.text = String(miner_count)

func update_items_inv():
	var available_buttons: Dictionary
	for btn in ItemBar.get_children():
		available_buttons[btn.name] = btn

	for item in PlayerRegistry.items:
		if not available_buttons.keys().has(item.name):
			var new_item: = ore_button_ui.instance()
			new_item.name = item.name
			new_item.texture_normal = item.icon
			new_item.label_name = item.name
			new_item.label_count = item.count
			new_item.connect("pressed", self, "_on_Item_pressed", [new_item, item])
			ItemBar.add_child(new_item)
		else:
			available_buttons[item.name].label_count = item.count

func _on_Miners_pressed():
	if miner_count > 0:
		item_menu.rect_global_position = MinerCount.get_parent().rect_global_position
		item_menu.target_name = "Miner"
		item_menu.popup()

func _on_Item_pressed(p_button: TextureButton, p_item: CraftableItem):
	if p_item.count > 0:
		item_menu.rect_global_position = p_button.rect_global_position
		item_menu.target_name = p_item.name
		item_menu.popup()
