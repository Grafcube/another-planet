extends Node

var registry: = {
	"Miner": preload("assets/icon_miner.svg"),
	"Iron": preload("assets/icon_iron.svg"),
	"Gold": preload("assets/icon_gold.svg"),
	"Rod": preload("assets/icon_rods.svg")
}
