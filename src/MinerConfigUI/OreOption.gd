extends TextureButton

export(String) var label_name: = "null" setget set_label_name
func set_label_name(p_name: String):
	if $Name:
		$Name.text = p_name
	label_name = p_name

export(int) var label_count: = 0 setget set_label_count
func set_label_count(p_count: int):
	if $Count:
		$Count.text = String(p_count)
	label_count = p_count
