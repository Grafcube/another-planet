extends Control

export(PackedScene) var ore_button_scn: PackedScene
var available_ores: Array # of Dictionaries
var ore_counts: Dictionary
var ores: Array # of ConstructionMaterial
var this_miner: Node
var this_planet: Node
var is_cfg: = false

func _ready():
	Alerts.set_alert("TIP: Choose the ore you want to mine from this planet")
	yield(get_tree().create_timer(1.0), "timeout")
	Alerts.set_alert("TIP: Press `ESC` to close Miner settings")

func _process(delta):
	ore_counts = this_planet.AvailableOres
	for item in $Window/Ores.get_children():
		item.label_count = ore_counts[item.name]
	$Window/Active/ActiveButton.pressed = this_miner.IsActive

func _input(event):
	if event.is_action_pressed("miner_cfg_exit"):
		for player in PlayerRegistry.registry:
			player.set_physics_process(true)
			player.set_process_unhandled_input(true)
		if not is_cfg:
			this_miner.get_parent().remove_child(this_miner)
		queue_free()

func get_ore(p_planet: Node, p_miner: Node):
	this_miner = p_miner
	this_planet = p_planet
	$Window/Active.hide()

	ore_counts = this_planet.AvailableOres
	for p in ore_counts.keys():
		available_ores.append( {"name": p, "count": ore_counts[p]} )
	for item in available_ores:
		var new_ore: = ConstructionMaterial.new()
		new_ore.name = item.name
		new_ore.count = item.count
		new_ore.icon = IconRegistry.registry[item.name]
		ores.append(new_ore)
	add_ore_options()

func configure_miner(p_planet, p_miner):
	this_miner = p_miner
	this_planet = p_planet

	ore_counts = this_planet.AvailableOres
	for p in ore_counts.keys():
		available_ores.append( {"name": p, "count": ore_counts[p]} )
	for item in available_ores:
		var new_ore: = ConstructionMaterial.new()
		new_ore.name = item.name
		new_ore.count = item.count
		new_ore.icon = IconRegistry.registry[item.name]
		ores.append(new_ore)
	add_ore_options(true)

func add_ore_options(p_config: = false):
	is_cfg = p_config
	for ore in ores:
		var option: = ore_button_scn.instance()
		option.name = ore.name
		option.texture_normal = ore.icon
		option.label_name = ore.name
		option.label_count = ore.count
		if this_miner.activeOreName == option.name:
			option.label_name += " (Active)"
		$Window/Ores.add_child(option)
		option.connect("pressed", self, "choose_ore", [option.name])
		for player in PlayerRegistry.registry:
			player.set_physics_process(false)
			player.set_process_unhandled_input(false)

func choose_ore(p_ore: String):
	for i in ores:
		if i.name == p_ore:
			this_miner.AssignOre(i.name, is_cfg)
			this_miner.IsActive = true
			break
	for player in PlayerRegistry.registry:
			player.set_physics_process(true)
			player.set_process_unhandled_input(true)
	queue_free()

func _on_ActiveButton_toggled(button_pressed: bool) -> void:
	if button_pressed:
		this_miner.IsActive = true
	else:
		this_miner.IsActive = false

func _on_PickUp_pressed():
	this_miner.IsActive = false
	this_miner.ResetMiner()
	PlayerRegistry.miners.append(this_miner)
	this_miner.get_parent().remove_child(this_miner)
	for player in PlayerRegistry.registry:
			player.set_physics_process(true)
			player.set_process_unhandled_input(true)
	queue_free()
