extends Control

func _input(event):
	if event.is_action_released("help_ui"):
		open_controls()

func open_controls():
	$Help.popup_centered()

func _on_pressed():
	open_controls()

func _on_mouse_entered():
	$TutorialContainer.modulate = Color("ffffffff")

func _on_mouse_exited():
	$TutorialContainer.modulate = Color("c0ffffff")

func _on_Mute_mouse_entered():
	$Mute.modulate = Color("ffffffff")

func _on_Mute_mouse_exited():
	$Mute.modulate = Color("c0ffffff")

func _on_Mute_toggled(button_pressed: bool):
	if button_pressed:
		$Mute.text = "Muted"
		get_node("/root/Main/AudioStreamPlayer").stream_paused = true
	else:
		$Mute.text = "Mute"
		get_node("/root/Main/AudioStreamPlayer").stream_paused = false
