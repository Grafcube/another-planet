extends Node

var AlertPanel: Panel

var is_available: = false
var alerts_arr: Dictionary

func _process(_delta):
	is_available = get_tree().root.has_node("/root/Main")
	if is_available:
		get_alerts_panel()

func get_alerts_panel():
	AlertPanel = get_node("/root/Main/HUD/Alerts")

func set_alert(p_text: String):
	if is_available:
		var log_id: = OS.get_unix_time()
		alerts_arr[log_id] = "> " + p_text
		AlertPanel.show()
		AlertPanel.get_node("Label").text = PoolStringArray(alerts_arr.values()).join("\n")
		AlertPanel.rect_size = AlertPanel.get_node("Label").rect_size
		yield(get_tree().create_timer(8.0), "timeout")
		alerts_arr.erase(log_id)
		if has_node_and_resource(@"/root/Main"):
			if alerts_arr.keys().size() == 0:
				AlertPanel.get_node("Label").text = ""
				AlertPanel.rect_size = Vector2(160, 21)
				AlertPanel.hide()
			else:
				AlertPanel.get_node("Label").text = PoolStringArray(alerts_arr.values()).join("\n")
				AlertPanel.rect_size = AlertPanel.get_node("Label").rect_size
