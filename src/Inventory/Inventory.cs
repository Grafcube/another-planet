using System.Collections.Generic;
using Godot;
using static Godot.GD;

namespace AnotherPlanet
{
	public class Inventory : Resource
	{
		private PackedScene MinerScene = (PackedScene)Load(@"res://src/Miner/Miner.tscn");

		public List<ConstructionMaterial> Ores = new List<ConstructionMaterial>();
		public List<Node> Miners = new List<Node>();
		public List<CraftableItem> Items = new List<CraftableItem>();

		public void ReplaceOres(List<Dictionary<string, string>> newInv)
		{
			Ores = default;
			foreach (var ore in newInv)
			{
				var newOre = new ConstructionMaterial
				{
					Name = ore["name"],
					Count = ore["count"].ToInt()
				};
				Ores.Add(newOre);
			}
		}

		public void ReplaceItems(List<Dictionary<string, string>> newItems)
		{
			Items = default;
			foreach (var item in newItems)
			{
				var newItem = new ConstructionMaterial
				{
					Name = item["name"],
					Count = item["count"].ToInt()
				};
				Ores.Add(newItem);
			}
		}

		public void AddMiner()
		{
			Miners.Add(MinerScene.Instance());
		}
	}
}
