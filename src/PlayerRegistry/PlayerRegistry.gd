extends Node

var registry: Array # of Nodes
var ores: Array setget ,get_ores # of ConstructionMaterials
func get_ores():
	return ores

var items: Array setget ,get_items # of CraftableItems
func get_items():
	return items

func get_item_dict():
	var result: Dictionary
	for i in items:
		result[i.name] = i.count
	return result

var miners: Array setget ,get_miners # of Nodes
func get_miners():
	return miners

var miner_scene: = preload("res://src/Miner/Miner.tscn")

func add_miner():
	var new_miner: = miner_scene.instance()
	miners.append(new_miner)

func add_ore(p_name: String, p_count: int):
	var exists: = false
	for o in ores:
		if o.name == p_name:
			o.count += p_count
			exists = true
			return o
	if not exists:
		var new_ore: = ConstructionMaterial.new()
		new_ore.name = p_name
		new_ore.count = p_count
		new_ore.icon = IconRegistry.registry[p_name]
		ores.append(new_ore)
		return new_ore

func add_item(p_name: String, p_count: int):
	var exists: = false
	for i in items:
		if i.name == p_name:
			i.count += p_count
			exists = true
			return i
	if not exists:
		var new_item: = CraftableItem.new()
		new_item.name = p_name
		new_item.count = p_count
		new_item.icon = IconRegistry.registry[p_name]
		items.append(new_item)
		return new_item

func register(p_player: Node):
	registry.append(p_player)

func deregister(p_player: Node):
	registry.erase(p_player)

func update_inventory(p_inv: Array):
	ores.clear()
	for ore in p_inv:
		var newOre: = ConstructionMaterial.new()
		newOre.name = ore.name
		newOre.count = int(ore.count)
		ores.append(newOre)

func update_items(p_items: Array):
	items.clear()
	for item in p_items:
		var newItem: = CraftableItem.new()
		newItem.name = item.name
		newItem.count = int(item.count)
		items.append(newItem)
