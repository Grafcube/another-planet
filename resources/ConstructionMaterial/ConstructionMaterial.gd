extends Resource
class_name ConstructionMaterial

export var name: = "null" setget ,get_name
func get_name():
	return name

export(int) var count = 0 setget set_count, get_count
func set_count(p_count: int):
	if p_count < 0:
		count = 0
	else:
		count = p_count
func get_count():
	return count

export(Texture) var icon: Texture

func _init(p_name: = "null", p_count: = 0, p_icon: = load("res://icon.png")) -> void:
	name = p_name
	set_count(p_count)
	icon = p_icon

func _ready():
	if icon == null:
		icon = load("res://icon.png")

func add_count(p_val: int):
	set_count(count + p_val)
