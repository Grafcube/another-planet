using System;
using Godot;

namespace AnotherPlanet
{
	public class ConstructionMaterial : Resource
	{
		[Export]
		public string Name = "null";

		private int _count = 0;

		[Export]
		public int Count
		{
			get { return _count; }
			set
			{
				if (value < 0)
				{
					_count = 0;
				}
				else
				{
					_count = value;
				}
			}
		}

		public ConstructionMaterial(string name = "Unknown ore", int count = 0)
		{
			Name = name;
			Count = count;
		}

		public static ConstructionMaterial operator +(ConstructionMaterial a, ConstructionMaterial b)
		{
			if (a.Name == b.Name)
			{
				return new ConstructionMaterial
				{
					Name = a.Name,
					Count = a.Count + b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static ConstructionMaterial operator -(ConstructionMaterial a, ConstructionMaterial b)
		{
			if (a.Name == b.Name)
			{
				return new ConstructionMaterial
				{
					Name = a.Name,
					Count = a.Count - b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static ConstructionMaterial operator *(ConstructionMaterial a, ConstructionMaterial b)
		{
			if (a.Name == b.Name)
			{
				return new ConstructionMaterial
				{
					Name = a.Name,
					Count = a.Count * b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static ConstructionMaterial operator /(ConstructionMaterial a, ConstructionMaterial b)
		{
			if (a.Name == b.Name)
			{
				return new ConstructionMaterial
				{
					Name = a.Name,
					Count = a.Count / b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static ConstructionMaterial operator %(ConstructionMaterial a, ConstructionMaterial b)
		{
			if (a.Name == b.Name)
			{
				return new ConstructionMaterial
				{
					Name = a.Name,
					Count = a.Count % b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}
	}
}
