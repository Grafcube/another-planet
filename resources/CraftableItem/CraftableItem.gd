extends Resource
class_name CraftableItem

export var name: = "null"
export(int) var count = 0 setget set_count
export(Texture) var icon: Texture

func set_count(p_count: int):
	if p_count < 0:
		count = 0
	else:
		count = p_count

func _init(p_name: = "null", p_count: = 0, p_icon: = load("res://icon.png")) -> void:
	name = p_name
	set_count(p_count)
	icon = p_icon

func _ready():
	if icon == null:
		icon = load("res://icon.png")
