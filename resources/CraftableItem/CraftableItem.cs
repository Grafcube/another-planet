using System;
using Godot;

namespace AnotherPlanet
{
	public class CraftableItem : Resource
	{
		[Export]
		public string Name = "null";

		private int _count = 0;

		[Export]
		public int Count
		{
			get { return _count; }
			set
			{
				if (value < 0)
				{
					_count = 0;
				}
				else
				{
					_count = value;
				}
			}
		}

		public CraftableItem(string name = "Unknown ore", int count = 0)
		{
			Name = name;
			Count = count;
		}

		public static CraftableItem operator +(CraftableItem a, CraftableItem b)
		{
			if (a.Name == b.Name)
			{
				return new CraftableItem
				{
					Name = a.Name,
					Count = a.Count + b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static CraftableItem operator -(CraftableItem a, CraftableItem b)
		{
			if (a.Name == b.Name)
			{
				return new CraftableItem
				{
					Name = a.Name,
					Count = a.Count - b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static CraftableItem operator *(CraftableItem a, CraftableItem b)
		{
			if (a.Name == b.Name)
			{
				return new CraftableItem
				{
					Name = a.Name,
					Count = a.Count * b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static CraftableItem operator /(CraftableItem a, CraftableItem b)
		{
			if (a.Name == b.Name)
			{
				return new CraftableItem
				{
					Name = a.Name,
					Count = a.Count / b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}

		public static CraftableItem operator %(CraftableItem a, CraftableItem b)
		{
			if (a.Name == b.Name)
			{
				return new CraftableItem
				{
					Name = a.Name,
					Count = a.Count % b.Count
				};
			}
			else
			{
				throw new ArgumentException("Both ores must have same name");
			}
		}
	}
}
